# lc3vm #

LC-3 Virtual Machine implemented in C. 

It is based on awesome blog post at https://www.jmeiners.com/lc3-vm/ by Justin Meiners and Ryan Pendleton.

Detailed specification on the machine can also be found at the https://www.jmeiners.com/lc3-vm/supplies/lc3-isa.pdf 

Requirements
* C compiler
* posix shell (optional) for build script

In order to run the examples
## Linux
```shell
	./vm 2048.obj 2> log
```
## Windows
```batch
.\vm.exe 2048.obj 2> log.txt
```

