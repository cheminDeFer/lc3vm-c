#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <assert.h>
#include <signal.h>

#ifdef __linux__
/* unix only */
#include <unistd.h>
#include <fcntl.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/termios.h>
#include <sys/mman.h>
#elif _WIN32
#include <Windows.h>
#include <conio.h>  // _kbhit
#else
#error "Unsupported os"
#endif

#define MEMORY_MAX (1 << 16)
uint16_t memory[MEMORY_MAX];
enum {
    R_R0,
    R_R1,
    R_R2,
    R_R3,
    R_R4,
    R_R5,
    R_R6,
    R_R7,
    R_PC,
    R_COND,
    R_COUNT
};

uint16_t reg[R_COUNT];
enum {
    OP_BR = 0, /* branch */
    OP_ADD,    /* add  */
    OP_LD,     /* load */
    OP_ST,     /* store */
    OP_JSR,    /* jump register */
    OP_AND,    /* bitwise and */
    OP_LDR,    /* load register */
    OP_STR,    /* store register */
    OP_RTI,    /* unused */
    OP_NOT,    /* bitwise not */
    OP_LDI,    /* load indirect */
    OP_STI,    /* store indirect */
    OP_JMP,    /* jump */
    OP_RES,    /* reserved (unused) */
    OP_LEA,    /* load effective address */
    OP_TRAP    /* execute trap 15 */
};

enum {
    FL_POS = (1 << 0),
    FL_ZRO = (1 << 1),
    FL_NEG = (1 << 2)
};

uint16_t
sign_extend (uint16_t x, int bit_count)
{
    if ((x >> (bit_count - 1)) & 0x1) {
        x |= (0xFFFF << bit_count);
    }
    return x;
}

void
update_flags (uint16_t r)
{
    if (reg[r] == 0)
        reg[R_COND] = FL_ZRO;
    else if (reg[r] >> 15)
        reg[R_COND] = FL_NEG;
    else
        reg[R_COND] = FL_POS;
}

void
mem_write (uint16_t addr, uint16_t val)
{
    memory[addr] = val;
}

enum {
    MR_KBSR = 0xFE00,
    MR_KBDR = 0xFE02
};


#ifdef __linux__
struct termios original_tio;

void
disable_input_buffering()
{
    tcgetattr (STDIN_FILENO, &original_tio);
    struct termios new_tio = original_tio;
    new_tio.c_lflag &= ~ICANON & ~ECHO;
    tcsetattr (STDIN_FILENO, TCSANOW, &new_tio);
}

void
restore_input_buffering()
{
    tcsetattr (STDIN_FILENO, TCSANOW, &original_tio);
}

uint16_t
check_key()
{
    fd_set readfds;
    FD_ZERO (&readfds);
    FD_SET (STDIN_FILENO, &readfds);
    struct timeval timeout;
    timeout.tv_sec = 0;
    timeout.tv_usec = 0;
    return select (1, &readfds, NULL, NULL, &timeout) != 0;
}
#elif _WIN32
HANDLE hStdin = INVALID_HANDLE_VALUE;
DWORD fdwMode, fdwOldMode;

void
disable_input_buffering()
{
    hStdin = GetStdHandle (STD_INPUT_HANDLE);
    GetConsoleMode (hStdin, &fdwOldMode); /* save old mode */
    fdwMode = fdwOldMode
              ^ ENABLE_ECHO_INPUT  /* no input echo */
              ^ ENABLE_LINE_INPUT; /* return when one or
                more characters are available */
    SetConsoleMode (hStdin, fdwMode); /* set new mode */
    FlushConsoleInputBuffer (hStdin); /* clear buffer */
}

void
restore_input_buffering()
{
    SetConsoleMode (hStdin, fdwOldMode);
}

uint16_t
check_key()
{
    return WaitForSingleObject (hStdin, 1000) == WAIT_OBJECT_0 && _kbhit();
}

#else
#error "Unsupported os"
#endif

uint16_t
mem_read (uint16_t addr)
{
    if (addr == MR_KBSR) {
        if (check_key()) {
            memory[MR_KBSR] = (1 << 15);
            memory[MR_KBDR] = getchar();
        } else {
            memory[MR_KBSR] = 0;
        }
    }
    return memory[addr];
}
uint16_t
swap (uint16_t x)
{
    return (x << 8) | (x >> 8);
}
uint16_t
read_image_file (FILE *f)
{
    uint16_t origin;
    if (fread (&origin, sizeof origin, 1, f) == 0)
        return 0;
    origin = swap (origin);
    uint16_t max_read = MEMORY_MAX - origin;
    uint16_t *p = memory + origin;
    uint16_t n_of_instr_read = fread (p, sizeof (uint16_t), max_read, f);
    if (!n_of_instr_read) {
        return 0;
    }
    while (n_of_instr_read-- > 0) {
        *p = swap (*p);
        p++;
    }
    return 1;
}

uint16_t
read_image (char *path)
{
    FILE *f = fopen (path, "rb");
    if (f == NULL) {
        return 0;
    }
    uint16_t res = read_image_file (f);
    fclose (f);
    return res;
}


enum {
    TRAP_GETC = 0X20,
    TRAP_OUT = 0X21,
    TRAP_PUTS = 0X22,
    TRAP_IN = 0X23,
    TRAP_PUTSP = 0X24,
    TRAP_HALT = 0X25
};

void
print_state (void)
{
    for (int i = 0; i < R_COUNT; ++i) {
        printf ("reg%d: %d ", i, reg[i]);
    }
    puts ("");
}

void
handle_interrupt (int signal)
{
    (void)signal;
    puts ("sig recieved");
    restore_input_buffering();
    printf ("\n");
    exit (-2);
}

int
main (int argc, char *argv[])
{
    signal (SIGINT, handle_interrupt);
    if (argc < 2) {
        fprintf (stderr, "%s\n", "Usage: vm [image1] ...");
        return 1;
    }
    for (int i = 1; i < argc; ++i) {
        if (!read_image (argv[i])) {
            fprintf (stderr, "Error: %s <%s>\n", "cannot read file: ", argv[i]);
            return 2;
        }
    }
    disable_input_buffering();
    reg[R_COND] = FL_ZRO;
    enum {
        PC_START = 0x3000
    };
    reg[R_PC] = PC_START;
    int running = 1;
    while (running) {
        uint16_t instr = mem_read (reg[R_PC]++);
        uint16_t op = instr >> 12;
        fprintf (stderr, "op:%4d instr: %04x\n", op, instr);
        switch (op) {
        case OP_BR: { /* branch */
            uint16_t cflag = (instr >> 9) & 0x7;
            if (cflag & reg[R_COND]) {
                uint16_t pcoffset9 =  sign_extend (instr & 0x1FF, 9);
                reg[R_PC] += pcoffset9;
            }
        }
        break;
        case OP_ADD: {  /* add  */
            uint16_t r0 = (instr >> 9) & 0x7;
            uint16_t r1 = (instr >> 6) & 0x7;
            uint16_t imm_flag = (instr >> 5) & 0x1;
            if (imm_flag) {
                uint16_t imm5 = sign_extend (instr & 0x1F, 5);
                reg[r0] = reg[r1] + imm5;
            } else {
                uint16_t r2 = instr & 0x7;
                reg[r0] = reg[r1] + reg[r2];
            }
            update_flags (r0);
        }
        break;
        case OP_LD:  {   /* load */
            uint16_t dr = ( instr >> 9) & 0x7;
            uint16_t pcoffset9 = sign_extend (instr & 0x1FF, 9);
            reg[dr] = mem_read (reg[R_PC] + pcoffset9);
            update_flags (dr);
        }
        break;
        case OP_ST: { /* store */
            uint16_t sr = (instr >> 9) & 0x7;
            uint16_t pcoffset9 = sign_extend (instr & 0x1FF, 9);
            mem_write (reg[R_PC] + pcoffset9, reg[sr]);
        }
        break;
        case OP_JSR: {  /* jump register */
            reg[R_R7] = reg[R_PC];
            if ((instr >> 11 ) & 0x1) {
                uint16_t pcoffset11 = sign_extend (instr & 0x7FF, 11);
                reg[R_PC] += pcoffset11;
            } else {
                uint16_t baser = (instr >> 6) & 0x7;
                reg[R_PC] = reg[baser];
            }
        }
        break;
        case OP_AND: {  /* bitwise and */
            uint16_t dr = (instr >> 9) & 0x7;
            uint16_t imm_flag = (instr >> 5) & 0x1;
            uint16_t sr1 = (instr >> 6) & 0x7;
            if (imm_flag) {
                reg[dr] = reg[sr1] & sign_extend (instr & 0x1F, 5);
            } else {
                uint16_t sr2 = instr & 0x7;
                reg[dr] = reg[sr1] & reg[sr2];
            }
            update_flags (dr);
        }
        break;
        case OP_LDR: {    /* load register */
            uint16_t dr = (instr >> 9) & 0x7;
            uint16_t baser = (instr >> 6) & 0x7;
            uint16_t offset6 = sign_extend (instr & 0x3F, 6);
            reg[dr] = mem_read (reg[baser] + offset6);
            update_flags (dr);
        }
        break;
        case OP_STR: {    /* store register */
            uint16_t sr = (instr >> 9) & 0x7;
            uint16_t baser = (instr >> 6) & 0x7;
            uint16_t pcoffset6 = sign_extend (instr & 0x3F, 6);
            mem_write (reg[baser] + pcoffset6, reg[sr]);
        }
        break;
        case OP_RTI: {   /* unused */
            abort();
        }
        break;
        case OP_NOT: {   /* bitwise not */
            uint16_t dr = (instr >> 9) & 0x7;
            uint16_t sr = (instr >> 6) & 0x7;
            reg[dr] = ~reg[sr];
            update_flags (dr);
        }
        break;
        case OP_LDI: {  /* load indirect */
            uint16_t dr = (instr >> 9) & 0x7;
            uint16_t pcoffset9 = sign_extend ((instr & 0x1FF), 9);
            reg[dr] = mem_read (mem_read (reg[R_PC] + pcoffset9));
            update_flags (dr);
        }
        break;
        case OP_STI: {   /* store indirect */
            uint16_t sr = (instr >> 9) & 0x7;
            uint16_t pcoffset9 = sign_extend (instr & 0x1FF, 9);
            mem_write (mem_read (reg[R_PC] + pcoffset9), reg[sr]);
        }
        break;
        case OP_JMP: {   /* jump */
            reg[R_PC] = reg[ (instr >> 6) & 0x7];
        }
        break;
        case OP_RES: {    /* reserved (unused) */
            abort();
        }
        break;
        case OP_LEA: {   /* load effective address */
            uint16_t dr = ( instr >> 9) & 0x7;
            uint16_t pcoffset9 = sign_extend (instr & 0x1FF, 9);
            reg[dr] = reg[R_PC] + pcoffset9;
            update_flags (dr);
        }
        break;
        case OP_TRAP: { /* execute trap */
            uint16_t trapvect8 = instr & 0xFF;
            switch (trapvect8) {
            case TRAP_GETC: {
                int c = getchar();
                if (!c) puts ("no char");
                reg[R_R0] = c;
                update_flags (R_R0);
            }
            break;
            case TRAP_OUT: {
                putc ((char)reg[R_R0], stdout);
                fflush (stdout);
            }
            break;
            case TRAP_PUTS: {
                uint16_t *c = memory + reg[R_R0];
                while (*c) {
                    putc ((char) *c, stdout);
                    ++c;
                }
                fflush (stdout);
            }
            break;
            case TRAP_IN: {
                puts ("Enter a character");
                fflush (stdout);
                char c = getchar();
                putc ((char)c, stdout);
                fflush (stdout);
                reg[R_R0] = (uint16_t)c;
                update_flags (R_R0);
            }
            break;
            case TRAP_PUTSP: {
                uint16_t *w =  (memory + reg[R_R0]);
                while (*w) {
                    char c1 = (char) (*w & 0xFF);
                    putc (c1 & 0xFF, stdout);
                    char c2 = (char) (*w >> 8);
                    if (c2)
                        putc (c2,  stdout);
                    ++w;
                }
                fflush (stdout);
            }
            break;
            case TRAP_HALT: {
                fflush (stdout);
                puts ("HALT");
                print_state();
                running = 0;
            }
            break;
            default: {
                assert (0 && "unexpected trap routine");
            }
            break;
            }
        }
        break;
        default: {
            assert (0 && "unexpected op code");
        }
        break;
        }
    }
    restore_input_buffering();
    return 0;
}
