#!/bin/sh

set -xe

CC=gcc
CFLAGS='-Wall -Wextra -Werror -pedantic -g'
# shellcheck disable=SC2086
# I want to split in here
${CC} ${CFLAGS}  -o vm main.c
