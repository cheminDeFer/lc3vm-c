with import <nixpkgs> {}; rec{
  lc3-env = pkgs.mkShell {
    name = "lc3-vm";
    nativeBuildInputs = [gdb];
    hardeningDisable = [ "all" ];
    buildInputs = [];
  };
}